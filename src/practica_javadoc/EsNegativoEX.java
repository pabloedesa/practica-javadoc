package practica_javadoc;

/**
 * Imprime un mensaje de error.
 * Esta clase advierte del error producido si se introduce un valor negativo. 
 * @author Creador García
 * @version 2.0
 */
public class EsNegativoEX extends Exception {
/**
 * Imprime el mensaje de error por defecto.
 */
public EsNegativoEX() {
super("el valor no puede ser negativo");
}
/**
 * Imprime el mensaje de error personalizado. <br>
 * Este mensaje se pasa por parámetros.
 * @param msg mensaje de error especial de cada método.
 */
public EsNegativoEX(String msg) {
super(msg);
}
}
