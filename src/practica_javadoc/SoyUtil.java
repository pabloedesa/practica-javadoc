package practica_javadoc;

/**
 * Una clase para obtener el factorial de un número, comprobar si es capicua o comprobar si es primo. <br>
 * Dispone de un método para cada comprobación.
 * @author Creador García
 * @version 2.0
 */
public class SoyUtil {
    /**
     * Comprueba si un número es capicúa. <br>
     * Este algoritmo utiliza enteros para hacer la comprobación
     * @param numero a consultar
     * @return true si es capicua o false si no lo es
     * @throws EsNegativoEX si el valor introducido es negartivo.
     * @see EsNegativoEX
     * @deprecated no se usa desde la versión 1.35 y reemplazado por el método {@link SoyUtil#esCapikua(int)}
     */
    public static boolean esCapicua(int numero) throws EsNegativoEX {
        if (numero < 0) {
            throw new EsNegativoEX();
        }
        int numAlReves = 0;
        int copia = numero;
        while (numero > 0) {
            numAlReves = numAlReves * 10 + numero % 10;
            numero /= 10;
        }
        return copia == numAlReves;
    }

    /**
     * Comprueba si un n&uacute;mero es capic&uacute;a. <br>
     * Convierte el par&aacute;metro a String y le da la vuelta para hacer la comprobaci&oacute;n.
     * @param numero a consultar
     * @return true si es capicua o false si no lo es
     * @throws EsNegativoEX si el valor introducido es negartivo.
     */
    public static boolean esCapikua(int numero) throws EsNegativoEX {
        if (numero < 0) {
            throw new EsNegativoEX();
        }
        String cadNum = numero + "";
        String numAlReves = (new StringBuilder(cadNum)).reverse().toString();
        return cadNum.equals(numAlReves);
    }

    /**
     * Comprueba si un número es primo.
     * @param numero a consultar
     * @return true si es primo o false si no lo es
     * @throws EsNegativoEX si el valor introducido es negartivo.
     */
    public static boolean esPrimo(int numero) throws EsNegativoEX {
        if (numero < 0) {
            throw new EsNegativoEX();
        }
        int limite = numero / 2 + 1;
        int div = 2;
        while (div < limite) {
            if (numero % div == 0) {
                return false;
            }
            div++;
        }
        return true;
    }

    /**
     * Calcula el factorial de un número.
     * @param numero a consultar
     * @return devuelve el factorial de este número.
     * @throws EsNegativoEX si el valor introducido es negartivo.
     */
    public static long getFactorial(int numero) throws EsNegativoEX {
        if (numero < 0) {
            throw new EsNegativoEX(
                    "no se puede calcular el factorial de un número negativo");
        }
        long fact = 1L;
        while (numero > 1) {
            fact *= numero;
            numero--;
        }
        return fact;
    }
}
