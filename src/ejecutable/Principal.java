package ejecutable;

import practica_javadoc.EsNegativoEX;
import practica_javadoc.SoyUtil;

public class Principal {
    public static void main(String[] args) {
/**
 * @throws IllegalArgumentException si el valor introducido es negartivo.
 */
        try {
            System.out.println("12345 es capicúa: " + SoyUtil.esCapikua(12345));
            System.out.println("1221 es capicúa: " + SoyUtil.esCapicua(1221));
            System.out.println("1234321 es capicúa: " + SoyUtil.esCapikua(1234321));
        } catch (EsNegativoEX ex) {
        }
    }
}
